from __future__ import unicode_literals
import frappe
from frappe import _

@frappe.whitelist()
def get_permission_query_condition_for_timesheet(user):
	if user != "Administrator" and "Operations Manager" in frappe.get_roles():
		return 0;
	elif user != "Administrator" and "Projects Manager" in frappe.get_roles():
		return 0;
	elif user != "Administrator" and "Resource Manager" in frappe.get_roles():
		return 0;
	elif user != "Administrator" and "Project Lead" in frappe.get_roles():
		return 0;
	elif user == "Administrator":
		return 0;
	else:
		return "`tabTimesheet`.owner = '{0}' or `tabTimesheet`._assign like '%%{0}%%'".format(user);
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "excelize"
app_title = "Excelize"
app_publisher = "Indictrans"
app_description = "Excelize"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "contact@indictranstech.com"
app_license = "MIT"

# Includes in <head>
# ------------------
fixtures = ['Custom Field','Property Setter','Role']
# include js, css files in header of desk.html
# app_include_css = "/assets/excelize/css/excelize.css"
# app_include_js = "/assets/excelize/js/excelize.js"

# include js, css files in header of web template
# web_include_css = "/assets/excelize/css/excelize.css"
# web_include_js = "/assets/excelize/js/excelize.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
doctype_js = {
	"Project": "excelize/custom_excelize/project/project.js",
	"Task": "excelize/custom_excelize/task/task.js"	
}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "excelize.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "excelize.install.before_install"
# after_install = "excelize.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "excelize.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

permission_query_conditions = {
	"Project": "excelize.excelize.custom_excelize.project.project.get_permission_query_condition_for_project",
	"Task": "excelize.excelize.custom_excelize.task.task.get_permission_query_condition_for_task",
	"Timesheet": "excelize.excelize.custom_excelize.timesheet.timesheet.get_permission_query_condition_for_timesheet"
}
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

doc_events = {
"Project": {
		"validate":[
		    	"excelize.excelize.custom_excelize.project.project.validate"
		    ]

	},
"Task": {
		"validate":[
		    	"excelize.excelize.custom_excelize.task.task.validate"
		    ]
	}
	
}

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"excelize.tasks.all"
# 	],
# 	"daily": [
# 		"excelize.tasks.daily"
# 	],
# 	"hourly": [
# 		"excelize.tasks.hourly"
# 	],
# 	"weekly": [
# 		"excelize.tasks.weekly"
# 	]
# 	"monthly": [
# 		"excelize.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "excelize.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "excelize.event.get_events"
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "excelize.task.get_dashboard_data"
# }

